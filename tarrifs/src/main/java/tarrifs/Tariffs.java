package tarrifs;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

public class Tariffs {

	public static void main(String[] args) {
		 try{
	            Class.forName("com.mysql.jdbc.Driver");
	            Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/billing2","root","root");
	            con.setAutoCommit(false);
	            PreparedStatement pstm = null ;
	            File myFile = new File("src/sample.xlsx");
	            FileInputStream fileInputStream = new FileInputStream(myFile);
	            Workbook wb = null;
	            
	            wb =new XSSFWorkbook(fileInputStream);
	            Sheet sheet = wb.getSheetAt(0);
	            Row row;
	            
	            pstm = (PreparedStatement) con.prepareStatement("DELETE FROM pattern_network");
	            int pNetworks= pstm.executeUpdate();
	            pstm=null;
	            System.out.println("deleted the pattern_network");
	            
	            
	            
	            pstm = (PreparedStatement) con.prepareStatement("DELETE FROM number_patterns where is_country=true");
	            int pattern= pstm.executeUpdate();
	            pstm=null;
	            System.out.println("deleted the numper_patterns");
	            
	            pstm = (PreparedStatement) con.prepareStatement("DELETE FROM networks where id <>500");
	            int networks= pstm.executeUpdate();
	            pstm=null;
	            System.out.println("deleted the networks");
	            
	            
	            pstm = (PreparedStatement) con.prepareStatement("DELETE FROM countries_dim where id <>500");
	            int countries= pstm.executeUpdate();
	            pstm =null;
	            System.out.println("deleted the countries");
	            
	            
	            
	            
	            
	            
	            
	            pstm = (PreparedStatement) con.prepareStatement("DELETE FROM rate where id >999");
	            int rates= pstm.executeUpdate();
	            pstm=null;
	            System.out.println("deleted the rates");
	            
	            
	            for(int i=1; i<=sheet.getLastRowNum(); i++){
	            	boolean flag = true;
	                row = sheet.getRow(i);
	                int id = (int)row.getCell(0).getNumericCellValue();
	                String code = row.getCell(1).getStringCellValue();
	                String country = row.getCell(2).getStringCellValue();
	                double rate = (double)row.getCell(3).getNumericCellValue();
	                String countriresInsert = "INSERT INTO countries_dim VALUES('"+id+"','"+country+"')";
	                String networkInsert =    "INSERT INTO networks VALUES('"+id+"','"+country+"','"+id+"')";
	                String rateInsert =       "INSERT INTO rate (id,followup_unit_cost,from_hour,initial_unit_cost,is_from_first_unit,number_units,time_unit,to_hour,version)"
	                		+ " VALUES(?,?,?,?,?,?,?,?,?)";
	                
	                String numberPatternInsert1 = "INSERT INTO number_patterns (id,comparison_type,is_country,length,name,starts_with,version,number_classification_id,rate_id,trunk_id,rate_plan_id)"
		               		+ "VALUES(?,?,?,?,?,?,?,?,?,?,?)";
	                
	              
	                String patternNetwork = "INSERT INTO pattern_network VALUES('"+id+"','"+(id+1000)+"')";
	                String secondpatternNetwork = "INSERT INTO pattern_network VALUES('"+id+"','"+(id+2000)+"')";
	                
	                pstm = (PreparedStatement) con.prepareStatement(countriresInsert);
	               pstm.execute();
	               
	                
	                pstm = (PreparedStatement) con.prepareStatement(networkInsert);
	               pstm.execute();
	                
	                
	                pstm = (PreparedStatement) con.prepareStatement(rateInsert);
	                pstm.setInt(1, id+1000);
	                pstm.setDouble(2,0.0);
	                pstm.setInt(3, 0);
	                pstm.setDouble(4,rate);
	                pstm.setBoolean(5, flag);
	                pstm.setInt(6, 1);
	                pstm.setInt(7,0);
	                pstm.setInt(8, 23 );
	                pstm.setInt(9, 1);
	                int affectedRows= pstm.executeUpdate();
	              
	                pstm =null;
	                
	                pstm = (PreparedStatement) con.prepareStatement(rateInsert);
	                pstm.setInt(1, id+2000);
	                pstm.setDouble(2,0.0);
	                pstm.setInt(3, 0);
	                pstm.setDouble(4,rate);
	                pstm.setBoolean(5, flag);
	                pstm.setInt(6, 1);
	                pstm.setInt(7,0);
	                pstm.setInt(8, 23 );
	                pstm.setInt(9, 1);
	               int affectedRows1= pstm.executeUpdate();
	              
	                
	                pstm = (PreparedStatement) con.prepareStatement(numberPatternInsert1);
	                pstm.setInt(1, id+1000);
	                pstm.setNull(2, java.sql.Types.INTEGER);
	                pstm.setBoolean(3, flag);
	                pstm.setNull(4, java.sql.Types.INTEGER);
	                pstm.setString(5, country);
	                pstm.setString(6, code);
	                pstm.setInt(7, 1);
	                pstm.setInt(8, 2);
	                pstm.setInt(9, id+1000);
	                pstm.setNull(10, java.sql.Types.INTEGER);
	                pstm.setInt(11, 1);
	                int a =pstm.executeUpdate();
	                
	                
	                pstm = (PreparedStatement) con.prepareStatement(numberPatternInsert1);
	                pstm.setInt(1, id+2000);
	                pstm.setNull(2, java.sql.Types.INTEGER);
	                pstm.setBoolean(3, flag);
	                pstm.setNull(4, java.sql.Types.INTEGER);
	                pstm.setString(5, country);
	                pstm.setString(6, code);
	                pstm.setInt(7, 1);
	                pstm.setInt(8, 2);
	                pstm.setInt(9, id+2000);
	                pstm.setNull(10, java.sql.Types.INTEGER);
	                pstm.setInt(11, 2);
	                int a1 =pstm.executeUpdate();
	                
	                pstm = (PreparedStatement) con.prepareStatement(patternNetwork);
	                pstm.execute();
	              
	                
	                pstm = (PreparedStatement) con.prepareStatement(secondpatternNetwork);
	                pstm.execute();
	              
	                
	                
	            }
	           wb.close();
	            con.commit();
	            pstm.close();
	            con.close();
	            fileInputStream.close();
	           
	            System.out.println("Success import excel to mysql table");
	        }catch(ClassNotFoundException e){
	            System.out.println(e);
	        }catch(SQLException ex){
	            System.out.println(ex);
	        }catch(IOException ioe){
	            System.out.println(ioe);
	        }
		
	}

}
